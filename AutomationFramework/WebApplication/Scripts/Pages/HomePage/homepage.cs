﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Threading;

namespace AutoTestFramework
{
    public class Homepage
    {        
        public static void Click_menu_item(string idvalue)
        {
            Thread.Sleep(2000);
            Driver.driverobj.FindElement(By.Id(idvalue)).Click();
        }
        public static void Select_user_role()
        {
            IWebElement dropdownmenu;
            IWebElement dropdownitems;
            
            dropdownmenu = Driver.driverobj.FindElement(By.Id("searchSystemUser_userType"));
                    

            for (int i=1; 1<=2; i++)
            {
                string dropdownelements = "#searchSystemUser_userType > option:nth-child("+ i +")";
                Thread.Sleep(2000);
                dropdownitems = Driver.driverobj.FindElement(By.CssSelector(dropdownelements));
                dropdownitems.Click();
                Console.WriteLine(dropdownitems.GetAttribute("value"));
                Driver.driverobj.FindElement(By.Id("searchBtn")).Click();
                Thread.Sleep(2000);
            }
        }
        public static void Employee_search(string name, string id, int expected_count)
        {
            int employee_count = 0;
            Driver.driverobj.FindElement(By.Id("menu_pim_viewPimModule")).Click();
            Thread.Sleep(3000);
            Driver.driverobj.FindElement(By.Id("menu_pim_viewEmployeeList")).Click();
            Thread.Sleep(2000);
            Driver.driverobj.FindElement(By.Name("empsearch[employee_name][empName]")).SendKeys(name);
            Driver.driverobj.FindElement(By.Id("empsearch_id")).SendKeys(id);
            Driver.driverobj.FindElement(By.Id("searchBtn")).Click();
            employee_count = Driver.driverobj.FindElements(By.XPath("//*[@id='resultTable']/tbody")).Count;
            Console.WriteLine(employee_count);
            Assert.IsTrue(employee_count==expected_count);
            Console.WriteLine("employee name search passed");
            
        }
        public static void Employee_search(string employment_status,int expected_count)
        {
            int employee_count = 0;
            Driver.driverobj.FindElement(By.Id("menu_pim_viewEmployeeList")).Click();
            Thread.Sleep(2000);
            Driver.driverobj.FindElement(By.Id("empsearch_employee_status")).SendKeys(employment_status);
            Driver.driverobj.FindElement(By.Id("searchBtn")).Click();
            employee_count = Driver.driverobj.FindElements(By.XPath("//*[@id='resultTable']/tbody")).Count;
            Console.WriteLine(employee_count);
            Assert.IsTrue(employee_count == expected_count);
            Console.WriteLine("employee_status search passed");
        }
    }
}
