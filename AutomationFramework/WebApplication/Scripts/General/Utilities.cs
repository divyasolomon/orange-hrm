﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using System;
using System.Reflection;
using System.Threading;
namespace AutomationFramework
{
    public class Utilities
    {
        public static IWebDriver driver;
        /*
        public static IEnumerable<String> BrowserToRunWith() //Open both browser 
        {
            string[] browsers = { "Chrome", "Firefox" };
            foreach (string BrowserName in browsers)
            {
                yield return BrowserName;
            }
        }*/
        //public string BrowserName = "Chrome"; //Run in declared browser
        public static void OpenBrowser(string BrowserName)
        {
            if (BrowserName.Equals("Chrome"))
            driver = new ChromeDriver();
            else if(BrowserName.Equals("Firefox"))
            driver = new FirefoxDriver();
            else
            driver = new ChromeDriver(); //Launches the Default Browser           
        }
        public static void GoToUrl()
        {
            driver.Navigate().GoToUrl(Constants.Url);
        }
        public static void Maximize_window()
        {
            driver.Manage().Window.Maximize();
        }                
        public static void ImplicitWait()
        {
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);
        }
        public static void WaitFor(int milliseconds)
        {
            Thread.Sleep(milliseconds);
        }
        public static void TakeScreenshot(string keyword)
        {
            var screenshot = ((ITakesScreenshot)driver).GetScreenshot();
            screenshot.SaveAsFile($"C:/selenium/{keyword}" + ".jpeg", ScreenshotImageFormat.Jpeg);
        }    
        public static void Quit()
        {
            driver.Quit();
        }
        public static string GetFilePath(string fileName)
        {
            string filePath = "";
            filePath = System.IO.Path.Combine(System.IO.Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), @"C:\Users\Divya.S\Desktop\Pdf old changes", fileName);
            Console.WriteLine(filePath);
            return filePath;            
        }  
    }
}
