﻿using AutoTestFramework;
using NUnit.Framework;

namespace AutomationFramework.WebApplication.Scripts.Tests
{
    [Parallelizable]
    class Test3 : BaseTest
    {
    [Test, Order(1)]
        public void Employee_search()
        {
            string employment_status = "Full-Time Permanent";
            string name = "Linda Anderson";
            string id = "0001";
            int expected_count = 1;
            Homepage.Employee_search(name, id, expected_count);
            Homepage.Employee_search(employment_status, expected_count);
        }


    }
}
