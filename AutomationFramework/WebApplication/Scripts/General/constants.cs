﻿using OpenQA.Selenium;
public class Constants
{
    //Login Credentails
    public static string Username = "admin";
    public static string Password = "admin123";
    public static string Url = "https://opensource-demo.orangehrmlive.com/index.php/auth/validateCredentials";
    
    public static By UsernameTxtbox = By.Name("txtUsername");
    public static By PasswordTxtbox = By.Id("txtPassword");
    public static By BtnLogin = By.ClassName("button");
    public static By Welcome = By.Id("welcome");

}
