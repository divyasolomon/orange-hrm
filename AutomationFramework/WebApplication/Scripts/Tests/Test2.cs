﻿using AutoTestFramework;
using NUnit.Framework;
using OpenQA.Selenium;

namespace AutomationFramework
{
    [Parallelizable]
    class Test2
    {
        IWebDriver driver;
        [OneTimeSetUp]
        public void Initialize()
        {
            driver.Navigate().GoToUrl("https://opensource-demo.orangehrmlive.com/index.php/auth/validateCredentials");

        }

        [Test, Order(1)]
        public void Login()
        {
            Loginconfig.login(driver);
        }

        [OneTimeTearDown]
        public void Cleanup()
        {
            Driver.Closeapplication(driver);
        }
    }
}
