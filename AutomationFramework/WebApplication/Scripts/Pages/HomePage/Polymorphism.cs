﻿using AutoTestFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutomationFramework.Master.Scripts.PageObjects.HomePage
{
    class Polymorphism
    {
        class ParentSearch
        {
            public void Search()
            {
                //Driver.driverobj.FindElement(By.Name("empsearch[employee_name][empName]")).SendKeys("Fiona Grace");
                Console.WriteLine("Default Vanilla Icecream");
            }
        }

        class PimEmployeeSearch:ParentSearch
        {
            public new void Search()
        {
            Console.WriteLine("Default Vanilla Icecream");
        }
    }

    class Directory:ParentSearch
    {
    // This is overridden method
    public new void Search()
    {
        Console.WriteLine("Vanilla with Chocolate Icecream");
    }
}











    }
}
