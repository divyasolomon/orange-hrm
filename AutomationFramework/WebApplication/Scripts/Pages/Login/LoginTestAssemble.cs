﻿using AutomationFramework;
using System;
using OpenQA.Selenium;
using Excel = Microsoft.Office.Interop.Excel;
public class Loginconfig : Utilities
{ /*
    public static void Login()
    {
        driver.FindElement(Constants.UsernameTxtbox).SendKeys(Constants.Username);
        driver.FindElement(Constants.PasswordTxtbox).SendKeys(Constants.Password);
        driver.FindElement(Constants.BtnLogin).Click();
        Assert.IsTrue(driver.FindElement(Constants.Welcome).Displayed);
        System.Console.WriteLine("Login is Successful");
    }*/

    Excel.Application Loginapp;
    Excel.Workbook Loginworkbook;
    Excel.Worksheet Loginworksheet;
    Excel.Range Loginrange;
    public Loginconfig()
    {
        Loginapp = new Excel.Application();
        Loginworkbook = Loginapp.Workbooks.Open(@"C:\Users\Divya.S\Downloads\Book1.xlsx");
        //loginworksheet = loginworkbook.Sheets[1];
        Loginworksheet = (Excel.Worksheet)Loginworkbook.Worksheets.get_Item(1);
        Loginrange = Loginworksheet.UsedRange;
    }
    public static void Login()
    {
        Loginconfig log = new Loginconfig();
        try
        {
            int emptyUsernameRowNumber = 1;
            int emptyUsernameColumnNumber = 2;            
            IWebElement username = driver.FindElement(Constants.UsernameTxtbox);
            username.Clear();
            username.SendKeys(log.Loginrange.Cells[emptyUsernameRowNumber][1].Value2.ToString());
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(1000);
            IWebElement password = driver.FindElement(Constants.PasswordTxtbox);
            password.Clear();
            password.SendKeys(log.Loginrange.Cells[emptyUsernameColumnNumber][1].Value2.ToString());
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(1000);
            driver.FindElement(Constants.BtnLogin).Click();
            NUnit.Framework.Assert.IsTrue(driver.FindElement(Constants.Welcome).Displayed);
            System.Console.WriteLine("Login is Successful");
        }
        catch (Exception e)
        {
            Console.Write(e);

        }
    }
}
