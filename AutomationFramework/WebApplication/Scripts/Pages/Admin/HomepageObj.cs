﻿using NUnit.Framework;
using OpenQA.Selenium;
using System;
using System.Threading;
using OpenQA.Selenium.Support.UI;
using AutomationFramework;

namespace AutoTestFramework
{
    public class Homepage : Utilities
    {
        public static void Click_menu_item(string idvalue)
        {
            driver.FindElement(By.Id(idvalue)).Click();
        }
        public static void ClickOnAddUserBtn()
        {
            driver.FindElement(By.Id("btnAdd")).Click();
        }
        public static void SelectUserRole(String DropdownValue)
        {
            //driver.FindElement(By.Id("systemUser_userType")).FindElement(By.XPath(".//option[contains(text(),'ESS')]")).Click();
            SelectElement Dropdown = new SelectElement(driver.FindElement(By.Id("systemUser_userType")));
            Dropdown.SelectByText(DropdownValue);
        }
        public static void SelectStatus(int index)
        {
            SelectElement Dropdown = new SelectElement(driver.FindElement(By.Id("systemUser_status")));
            Dropdown.SelectByIndex(index);
        }
        public static void EnterPassword(string password)
        {
            IWebElement passwordfield = driver.FindElement(By.Name("systemUser[password]"));
            passwordfield.Click();
                passwordfield.SendKeys(password);
        }
        public static void ConfirmPassword(string password)
        {
            driver.FindElement(By.Id("systemUser_confirmPassword")).SendKeys(password);
        }
        public static void Select_User(string checkbox_value)
        {
            SelectElement Checkbox = new SelectElement(driver.FindElement(By.Id("systemUser_userType")));
            Checkbox.SelectByValue(checkbox_value);
        }
        public static void Deletepopup()
        {
            Assert.AreEqual("OrangeHRM - Confirmation Required", driver.FindElement(By.XPath("//*[@id='deleteConfModal']/div[1]/h3")).GetAttribute("text"));
            driver.FindElement(By.Id("dialogDeleteBtn")).Click();
        }
        public static void ClickOnDeletebBtn()
        {
            driver.FindElement(By.Id("btnDelete")).Click();
        }
        public static void ClickOnSave()
        {
            driver.FindElement(By.Id("btnSave")).Click();
        }
        public static void UserSearch(string name)
        {
            driver.FindElement(By.Id("searchSystemUser_userName")).SendKeys(name);
            Thread.Sleep(1000);
            driver.FindElement(By.Id("searchBtn")).Click();            
        }
        public static void Employee_search(string name, string id, int expected_count)
        {
            int employee_count = 0;
            driver.FindElement(By.Id("menu_pim_viewPimModule")).Click();
            Thread.Sleep(3000);
            driver.FindElement(By.Id("menu_pim_viewEmployeeList")).Click();
            Thread.Sleep(2000);
            driver.FindElement(By.Name("empsearch[employee_name][empName]")).SendKeys(name);
            driver.FindElement(By.Id("empsearch_id")).SendKeys(id);
            driver.FindElement(By.Id("searchBtn")).Click();
            employee_count = driver.FindElements(By.XPath("//*[@id='resultTable']/tbody")).Count;
            Console.WriteLine(employee_count);
            Assert.IsTrue(employee_count==expected_count);
            Console.WriteLine("employee name search passed");            
        }
        public static void Employee_search(string employment_status,int expected_count)
        {
            int employee_count = 0;
            driver.FindElement(By.Id("menu_pim_viewEmployeeList")).Click();
            Thread.Sleep(2000);
            driver.FindElement(By.Id("empsearch_employee_status")).SendKeys(employment_status);
            driver.FindElement(By.Id("searchBtn")).Click();
            employee_count = driver.FindElements(By.XPath("//*[@id='resultTable']/tbody")).Count;
            Console.WriteLine(employee_count);
            Assert.IsTrue(employee_count == expected_count);
            Console.WriteLine("employee_status search passed");
        }
  
    }
}
