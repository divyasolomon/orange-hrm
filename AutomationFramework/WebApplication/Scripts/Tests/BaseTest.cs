﻿using NUnit.Framework;
namespace AutomationFramework.WebApplication.Scripts.Tests
{
    public class BaseTest : Loginconfig
    {  
        [OneTimeSetUp]
        public static void InitializeDriver()
        {
            string BrowserName = "Chrome";
            OpenBrowser(BrowserName);
            GoToUrl();
            Maximize_window();                   
            Login();            
        }
        [OneTimeTearDown]
        public static void Closeapplication()
        {
            Quit();
        }
    }
}
