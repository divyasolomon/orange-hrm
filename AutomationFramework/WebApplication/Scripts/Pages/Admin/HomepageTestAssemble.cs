﻿using AutoTestFramework;
using NUnit.Framework;
using OpenQA.Selenium;
using System;
using System.Threading;
namespace AutomationFramework
{
    public class TestAssemble : Homepage
    {        
        public static void AddUser()
        {
            string idvalue = "menu_admin_viewAdminModule";
            Click_menu_item(idvalue);
            ClickOnAddUserBtn();
            SelectUserRole("ESS");
            driver.FindElement(By.Id("systemUser_employeeName_empName")).SendKeys("Fiona Grace");
            driver.FindElement(By.Name("systemUser[userName]")).SendKeys("Fiona 909");
            SelectStatus(0);
            EnterPassword("ASQW12345");
            ConfirmPassword("ASQW12345");
            ClickOnSave();
            Thread.Sleep(5000);
            UserSearch("Fiona 909");
            Thread.Sleep(2000);
            IWebElement user_name = driver.FindElement(By.XPath("//*[@id='resultTable']/tbody/tr/td[2]/a"));
            Assert.AreEqual("Fiona 909", user_name.GetAttribute("text"));
            Console.WriteLine("User added successfully");
        }
        public static void DeleteUser()
        {
            Select_User("10");
            ClickOnDeletebBtn();
            Deletepopup();

        }

    }
}
