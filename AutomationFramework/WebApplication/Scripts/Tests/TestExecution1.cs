﻿using AutomationFramework;
using AutomationFramework.WebApplication.Scripts.Pages;
using AutomationFramework.WebApplication.Scripts.Tests;
using NUnit.Framework;
using System;
using System.Reflection;
namespace AutoTestFramework
{
    [Parallelizable]
    [TestFixture]
    public class TestExecution : BaseTest
    {   
        /*
        [Test, Order(1)]
        [TestCaseSource("BrowserToRunWith")]
        public void Login(string BrowserName)
        {        
        BaseTest.InitializeDriver(BrowserName);   
            
        }*/
        [Test, Order(1)]
        public void AddUser()
        {
            TestAssemble.AddUser();
        }
        [Test, Order(2)]
        public void DeleteUser1()
        {
            Homepage.Click_menu_item("menu_admin_viewAdminModule");
            TestAssemble.DeleteUser();
        }        
        [Test, Order(3)]
        public void Pdf1()
        {
            string Actual = "PT_non_guided.pdf";
            Actual = System.IO.Path.Combine(System.IO.Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), @"C:\Users\Divya.S\Desktop\Actual pdf", Actual);
            Console.WriteLine(Actual);
            string Expected = "PT non guided.pdf";
            Expected = System.IO.Path.Combine(System.IO.Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), @"C:\Users\Divya.S\Desktop\Expected pdf", Expected);
            Console.WriteLine(Expected);
            Pdf.CompareTwoPDF(Actual, Expected);          
        }
    }
}
